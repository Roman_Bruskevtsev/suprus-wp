'use strict';
var themeName               = 'suprus',
    gulp                    = require('gulp'),
    watch                   = require('gulp-watch'),
    sass                    = require('gulp-sass'),
    cssmin                  = require('gulp-cssmin'),
    imagemin                = require('gulp-imagemin'),
    imageminJpegRecompress  = require('imagemin-jpeg-recompress'),
    pngquant                = require('imagemin-pngquant'),
    babel                   = require('gulp-babel'),
    uglify                  = require('gulp-uglify-es').default,
    rename                  = require('gulp-rename'),
    concat                  = require('gulp-concat'),
    pipeline                = require('readable-stream').pipeline,
    scriptsArray            = [
        // themeName + '/assets/js/mousewheel-smoothscroll.js',
        themeName + '/assets/js/swiper-bundle.js',
        themeName + '/assets/js/aos.js',
        themeName + '/assets/js/lightgallery-all.js',
        themeName + '/assets/js/general.js'
    ],
    styleArray              = [
        themeName + '/assets/scss/**/*.scss'
    ];

gulp.task('styles',  function () {
    return gulp.src(themeName + '/assets/scss/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(themeName + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(themeName + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src(scriptsArray).pipe(concat('all.js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest(themeName + '/assets/js/'));
});

// gulp.task('images', function () {
//     return gulp.src(themeName + '/images/**/*.+(png|jpg|gif|svg)')
//         .pipe(imagemin([
//             imagemin.gifsicle({interlaced: true}),
//             imagemin.jpegtran({progressive: true}),
//             imageminJpegRecompress({
//                 loops: 5,
//                 min: 65,
//                 max: 70,
//                 quality:'medium'
//             }),
//             imagemin.svgo(),
//             imagemin.optipng({optimizationLevel: 3}),
//             pngquant({quality: [0.65,0.7], speed: 5})
//         ]))
//         .pipe(gulp.dest(themeName + '/assets/images'));
// });

gulp.task('default', gulp.series(
    gulp.parallel(
    'styles',
    'scripts',
    // 'images'
    )
));

gulp.task('watch', function () {
    gulp.watch(themeName + '/assets/scss/**/*.scss', gulp.series('styles'));

    gulp.watch(scriptsArray, gulp.series('scripts'));

    // gulp.watch(themeName + '/images/**/*.+(png|jpg|gif|svg)', gulp.series('images'));
});