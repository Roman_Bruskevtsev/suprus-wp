<?php

if ( file_exists( dirname( __FILE__ ) . '/config/dev.php' ) ) {
    /*Configuration for development*/
    include( dirname( __FILE__ ) . '/config/dev.php' );
} else if ( file_exists( dirname( __FILE__ ) . '/config/stg.php' ) ) {
    /*Configuration for staging*/
    include( dirname( __FILE__ ) . '/config/stg.php' );
} else {
    /*Configuration for production*/
    include( dirname( __FILE__ ) . '/config/prd.php' );
}

define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

$table_prefix  = 'wp_';

define( 'WPLANG', '' );

if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) )
    $memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );

define( 'WP_STAGE', '%%WP_STAGE%%' );
define( 'STAGING_DOMAIN', '%%WP_STAGING_DOMAIN%%' ); 

if ( !defined( 'ABSPATH' ) )
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
require_once( ABSPATH . 'wp-settings.php' );

/*Close admin panel for production*/
// if ( ENV == 'prd' ) {
//     if (function_exists('add_action')) {
//         add_action('admin_init', '__close_admin');
//         function __close_admin()
//         {
//             global $pagenow;

//             if ($pagenow != 'admin-ajax.php') {
//                 wp_die('admin dashboard is closed for production');
//             }
//         }
//     }
// }