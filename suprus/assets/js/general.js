'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
            general.headerAnimationInit();
            general.valuesSliderInit();
            general.testimonialsSliderInit();
            general.mobileNavigation();
        });
    }

    windowLoad(){
        window.onload = function() {
            general.sectionScrollInit();
        }
    }

    headerAnimationInit(){
        let header = document.querySelector('.spr-header'),
            lastScrollTop = 0,
            smoothMenuPoints = document.querySelectorAll('.spr-main__nav li a[href^="#"]'),
            smoothMobileMenuPoints = document.querySelectorAll('.spr-mobile__nav li a[href^="#"]'),
            st;

        window.addEventListener('scroll', () => {
            st = window.pageYOffset;
            
            if( st <= 0 ){
                header.classList.remove('scroll__down');
            } else if ( st > lastScrollTop && st > 0 ){
                header.classList.add('scroll__down');
            } else {
                header.classList.remove('scroll__down');
            }
            if( st > 100 ){
                header.classList.add('dark');
            } else {
                header.classList.remove('dark');
            }

            lastScrollTop = st;
        });

        if( smoothMenuPoints ){
            let headerHeight, pointHref, pointTopHeight, siteUrl, newUrl;
            smoothMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY - headerHeight;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);

                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });

            smoothMobileMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY - headerHeight;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);
                    
                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });
        }
    }

    mobileNavigation(){
        let btn = document.querySelector('.spr-mobile__btn'),
            header = document.querySelector('.spr-header'),
            menuPoints = document.querySelectorAll('.spr-mobile__nav a'),
            menu = document.querySelector('.spr-mobile__wrapper');
        if( !btn ) return false;
        btn.addEventListener('click', () => {
            btn.classList.toggle('show');
            header.classList.toggle('show');
            menu.classList.toggle('show');
        });

        menuPoints.forEach( (item) => {
            item.addEventListener('click', () => {
                menuPoints.forEach( (point) => {
                    point.classList.remove('active');
                });
                item.classList.add('active');
                header.classList.remove('show');
                btn.classList.remove('show');
                menu.classList.remove('show');
            });
        });
    }

    valuesSliderInit(){
        let sliderText = document.querySelector('.spr-values__slider__text'),
            sliderImage = document.querySelector('.spr-values__slider__image');
        if( !sliderText && !sliderImage ) return false;
        let imageSlider = new Swiper(sliderImage, {
                slidesPerView: 1,
                spaceBetween: 30,
                speed: 700,
                navigation: false,
                effect: 'fade',
            }),
            textSlider = new Swiper(sliderText, {
                slidesPerView: 1,
                spaceBetween: 30,
                speed: 700,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
        imageSlider.controller.control = textSlider;
        textSlider.controller.control = imageSlider;
    }

    testimonialsSliderInit(){
        let slider = document.querySelector('.spr-testimonials__slider');
        if( !slider ) return false;

        let testimonialsSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: 30,
            speed: 700,
            effect: 'fade',
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            }
        });
    }

    sectionScrollInit(){
        let scrollSections = document.querySelectorAll('.scroll__section'),
            menuItems = document.querySelectorAll('.spr-main__menu li'),
            mobileMenuItems = document.querySelectorAll('.spr-mobile__menu li'),
            topScroll, sectionScroll, sectionHeight, sectionActiveHeight, activeSection, linkHref;
        if(!scrollSections) return false;

        window.addEventListener('scroll', function() {
            topScroll = window.scrollY;
            scrollSections.forEach( (section) => {
                sectionScroll = section.getBoundingClientRect().top;

                if( sectionScroll <= 0 ) {
                    activeSection = '#' + section.getAttribute('id');

                    menuItems.forEach( (item) => {
                        linkHref = item.querySelector('a').getAttribute('href');
                        linkHref = '#' + linkHref.substring(linkHref.indexOf('#') + 1);

                        if( linkHref == activeSection ){
                            item.querySelector('a').classList.add('active');
                        } else {
                            item.querySelector('a').classList.remove('active');
                        }
                    });

                    mobileMenuItems.forEach( (item) => {
                        linkHref = item.querySelector('a').getAttribute('href');
                        linkHref = '#' + linkHref.substring(linkHref.indexOf('#') + 1);

                        if( linkHref == activeSection ){
                            item.querySelector('a').classList.add('active');
                        } else {
                            item.querySelector('a').classList.remove('active');
                        }
                    });
                }
            });
        });
    }
}

let general = new GeneralClass();