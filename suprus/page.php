<?php
/**
 * @package WordPress
 * @subpackage Suprus
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'hero_section' ): 
            get_template_part( 'template-parts/page/hero_section' );
        endif;
    endwhile;
endif;

get_footer();