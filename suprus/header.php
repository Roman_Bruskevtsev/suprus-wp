<?php
/**
 * @package WordPress
 * @subpackage Suprus
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="spr-header">
        <div class="container">
            <div class="row">
                <div class="col-8 col-lg-10">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="spr-logo float-start" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php }
                    if( has_nav_menu('main') ) { ?>
                    <div class="spr-main__menu float-start d-none d-lg-block">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'spr-main__nav'
                        ) ); ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-4 col-lg-2">
                    <?php if( has_nav_menu('main') ) { ?>
                    <div class="spr-mobile__btn float-end d-block d-lg-none">
                        <span></span>
                        <span></span>
                    </div>
                    <?php }
                    $languages = pll_the_languages( array('raw' => 1) );
                    if( $languages ){
                        $current_lang = $lang_list = '';
                        foreach ( $languages as $lang ) {
                            if( !$lang['current_lang'] ){
                                $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                            }
                        } ?>
                        <div class="spr-language__switcher float-end d-none d-lg-block">
                            <ul>
                                <?php echo $lang_list; ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>
    <div class="spr-mobile__wrapper d-block d-lg-none">
        <?php if( has_nav_menu('main') ) { ?>
        <div class="spr-mobile__menu">
            <?php wp_nav_menu( array(
                'theme_location'        => 'main',
                'container'             => 'nav',
                'container_class'       => 'spr-mobile__nav'
            ) ); ?>
        </div>
        <?php } 
        if( $languages ){
            $current_lang = $lang_list = '';
            foreach ( $languages as $lang ) {
                if( $lang['current_lang'] ){
                    $lang_list .= '<li><a class="active" href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                } else {
                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                }
            } ?>
            <div class="spr-language__switcher float-start">
                <ul>
                    <?php echo $lang_list; ?>
                </ul>
            </div>
        <?php } ?>
    </div>
    <main>