<?php
/**
 * @package WordPress
 * @subpackage Suprus
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) : the_post(); 

get_template_part( 'template-parts/post/content', 'post_banner' ); ?>

<section class="dns-post__content">
    <div class="container">
        <?php 
        if( have_rows('content') ):
            while ( have_rows('content') ) : the_row();
                if( get_row_layout() == 'text_image_section_half' ): 
                    get_template_part( 'template-parts/post/content', 'text_image_section_half' );
                elseif( get_row_layout() == 'image_text_section_half' ): 
                    get_template_part( 'template-parts/post/content', 'image_text_section_half' );
                elseif( get_row_layout() == 'full_image' ): 
                    get_template_part( 'template-parts/post/content', 'full_image' );
                elseif( get_row_layout() == 'text_logos_section_quoter' ): 
                    get_template_part( 'template-parts/post/content', 'text_logos_section_quoter' );
                endif;
            endwhile;
        endif; ?>
    </div>
</section>

<?php 

get_template_part( 'template-parts/post/content', 'related_posts' );

endwhile;

get_footer();