<?php
/**
 * @package WordPress
 * @subpackage Suprus
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'hero_section' ):
            get_template_part( 'template-parts/page/hero_section' );
        elseif( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'template-parts/page/about_us_section' );
        elseif( get_row_layout() == 'services_section' ): 
            get_template_part( 'template-parts/page/services_section' );
        elseif( get_row_layout() == 'offers_section' ): 
            get_template_part( 'template-parts/page/offers_section' );
        elseif( get_row_layout() == 'values_section' ): 
            get_template_part( 'template-parts/page/values_section' );
        elseif( get_row_layout() == 'values_slider_section' ): 
            get_template_part( 'template-parts/page/values_slider_section' );
        elseif( get_row_layout() == 'testimonials_section' ): 
            get_template_part( 'template-parts/page/testimonials_section' );
        endif;
    endwhile;
endif;

get_footer();