<?php
/**
 * @package WordPress
 * @subpackage Suprus
 * @since 1.0
 * @version 1.0
 */
$footer = get_field('footer', 'option');
?>  
    </main>
    <footer class="spr-footer scroll__section" id="contacts">
        <div class="container">
            <?php if( $footer['title'] ) { ?>
            <div class="row">
                <div class="col">
                    <div class="spr-footer__title" data-aos="fade-up" data-aos-delay="200">
                        <h2><?php echo $footer['title']; ?></h2>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <?php if( $footer['form_shortcode'] ) { ?>
                <div class="col-md-7">
                    <div class="spr-footer__form" data-aos="fade-up" data-aos-delay="200"><?php echo do_shortcode($footer['form_shortcode']); ?></div>
                </div>
                <?php }
                if( $footer['address'] || $footer['email'] ) { ?>
                <div class="col-md-1"></div>
                <div class="col-md-4" data-aos="fade-left" data-aos-delay="200">
                    <div class="spr-footer__title">
                        <h6><?php _e('Contacts', 'suprus'); ?></h6>
                    </div>
                    <?php if( $footer['address'] ) { ?>
                    <div class="spr-footer__address">
                        <p class="title"><?php _e('Location', 'suprus'); ?></p>
                        <?php echo $footer['address']; ?>
                    </div>
                    <?php }
                    if( $footer['email'] ) { ?>
                    <div class="spr-footer__email">
                        <p class="title"><?php _e('Email', 'suprus'); ?></p>
                        <a href="mailto:<?php echo $footer['email']; ?>"><?php echo $footer['email']; ?></a>
                    </div>
                    <?php }
                    if( get_field('logo', 'option') ) { ?>
                    <a class="spr-footer__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php }
                    if( $footer['copyrights'] ) { ?>
                    <div class="spr-footer__copyrights">
                        <p><?php echo $footer['copyrights']; ?></p>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>