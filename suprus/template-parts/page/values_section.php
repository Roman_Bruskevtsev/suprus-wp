<?php 
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<section class="spr-values__section scroll__section"<?php echo $background; echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<?php if( get_sub_field('small_title') || get_sub_field('title') || get_sub_field('text') ) { ?>
	<div class="container d-none d-md-block">
		<div class="row">
			<div class="col-md-8">
				<div class="spr-section__title white" data-aos="fade-up" data-aos-delay="200">
					<?php if( get_sub_field('small_title') ) { ?><h6><?php the_sub_field('small_title'); ?></h6><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="image d-block d-sm-none"<?php echo $background; ?>></div>
	<div class="spr-section__title white d-block d-md-none">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php if( get_sub_field('small_title') ) { ?><h6><?php the_sub_field('small_title'); ?></h6><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>