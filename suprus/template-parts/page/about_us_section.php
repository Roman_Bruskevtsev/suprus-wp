<section class="spr-about__us__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
			<?php if( get_sub_field('image') ) { ?>
				<div class="image" style="background-image: url('<?php the_sub_field('image'); ?>');"></div>
			<?php } ?>
			</div>
			<div class="col-md-1"></div>
			<?php if( get_sub_field('small_title') || get_sub_field('title') || get_sub_field('text') ) { ?>
			<div class="col-md-5">
				<div class="spr-section__title" data-aos="fade-up" data-aos-delay="200">
					<?php if( get_sub_field('small_title') ) { ?><h6><?php the_sub_field('small_title'); ?></h6><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
					<?php the_sub_field('text'); ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>