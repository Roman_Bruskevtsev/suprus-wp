<?php 
$slider = get_sub_field('slider');
if( $slider ) { ?>
<section class="spr-values__slider__section">
	<div class="container">
		<div class="row">
			<div class="col-md-6 order-md-2">
				<div class="spr-values__slider__image swiper">
					<div class="swiper-wrapper">
						<?php foreach ( $slider as $slide ) { ?>
						<div class="swiper-slide" style="background-image: url('<?php echo $slide['image']; ?>');"></div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-md-6 order-md-1">
				<div class="spr-values__slider__text swiper" data-aos="fade-up" data-aos-delay="200">
					<div class="swiper-wrapper">
						<?php foreach ( $slider as $slide ) { ?>
						<div class="swiper-slide">
							<div class="content text-center">
								<?php if( $slide['title'] ) { ?><h2><?php echo $slide['title']; ?></h2><?php }
								echo $slide['text']; ?>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>