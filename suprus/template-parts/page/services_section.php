<section class="spr-services__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('small_title') || get_sub_field('title') || get_sub_field('text') ) { ?>
			<div class="col-md-6">
				<div class="spr-section__title" data-aos="fade-up" data-aos-delay="200">
					<?php if( get_sub_field('small_title') ) { ?><h6><?php the_sub_field('small_title'); ?></h6><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
					<?php the_sub_field('text'); ?>
				</div>
			</div>
			<?php } ?>
			<div class="col-md-1"></div>
			<div class="col-md-4">
			<?php if( get_sub_field('image') ) { ?>
				<div class="image">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			<?php } ?>
			</div>
		</div>
		<?php 
		$services = get_sub_field('services');
		if( $services ) { ?>
		<div class="row">
			<?php foreach ( $services as $service ) { ?>
			<div class="col-md-4">
				<div class="spr-service__block text-center" data-aos="fade-up" data-aos-delay="200">
					<?php if( $service['title'] ) { ?><h6><?php echo $service['title']; ?></h6><?php } ?>
					<?php echo $service['text']; ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>