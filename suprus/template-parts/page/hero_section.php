<?php 
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<section class="spr-hero__section"<?php echo $background; ?>>
	<div class="content" data-aos="fade-up" data-aos-delay="200">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-11">
					<div class="row justify-content-center">
						<?php if( get_sub_field('small_title') ) { ?>
						<div class="col-lg-3">
							<div class="small__title">
								<h6><?php the_sub_field('small_title'); ?></h6>
							</div>
						</div>
						<?php }
						if( get_sub_field('title') ) { ?>
						<div class="col-lg-9">
							<div class="title">
								<h1><?php the_sub_field('title'); ?></h1>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>