<section class="spr-offers__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
			<div class="col-md-6">
				<div class="spr-section__title" data-aos="fade-up" data-aos-delay="200">
					<?php if( get_sub_field('small_title') ) { ?><h6><?php the_sub_field('small_title'); ?></h6><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
				</div>
			</div>
			<?php }
			if( get_sub_field('text') ) { ?>
			<div class="col-md-6">
				<div class="spr-section__text" data-aos="fade-up" data-aos-delay="200"><?php the_sub_field('text'); ?></div>
			</div>
			<?php } ?>
		</div>
		<?php 
		$offers = get_sub_field('offers');
		if( $offers ) { ?>
		<div class="row">
			<?php foreach ( $offers as $offer ) { ?>
			<div class="col-md-4">
				<div class="spr-offer__block text-center" data-aos="fade-up" data-aos-delay="200">
				<?php if( $offer['icon'] ) { ?>
					<div class="icon">
						<img src="<?php echo $offer['icon']['url']; ?>" alt="<?php echo $offer['icon']['title']; ?>">
					</div>
				<?php } 
				if( $offer['title'] ) { ?>
					<h6><?php echo $offer['title']; ?></h6>
				<?php } 
				echo $offer['text']; ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>