<section class="spr-testimonials__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('title') ) { ?>
			<div class="col-md-3">
				<div class="spr-section__title" data-aos="fade-right" data-aos-delay="200">
					<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
				</div>
			</div>
			<?php } 
			$slider = get_sub_field('slider'); 
			if( $slider ) { ?>
			<div class="col-md-1"></div>
			<div class="col-md-8">
				<div class="spr-testimonials__slider swiper" data-aos="fade-up" data-aos-delay="200">
					<div class="swiper-wrapper">
						<?php foreach ( $slider as $slide ) { ?>
						<div class="swiper-slide">
							<div class="content">
								<?php if( $slide['text'] ) { ?>
									<h5><?php echo $slide['text']; ?></h5>
								<?php } ?>
							</div>
							<?php if( $slide['author'] ) { ?><h5 class="author"><?php echo $slide['author']; ?></h5><?php } ?>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>